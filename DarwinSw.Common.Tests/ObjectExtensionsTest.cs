using NUnit.Framework;

namespace Darwin.Common.Tests {
    public class ObjectExtensionsTest {
        [Test]
        public void OrReturnsDefaultValueWhenSelfIsNull() {
            string text = null;
            string defaultValue = "a default value";
            Assert.That(text.Or(defaultValue), Is.EqualTo(defaultValue));
        }
        
        [Test]
        public void OrReturnsSelfWhenItIsNotNull() {
            string text = "The text";
            string defaultValue = "a default value";
            Assert.That(text.Or(defaultValue), Is.EqualTo(text));
        }
    }
}
