using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace Darwin.Common.Tests {
    public class EnumerableExtensionsTest {
        [Test]
        public void OrEmptyReturnsEmptyEnumerableWhenSelfIsNull() {
            IEnumerable<string> list = null;
            Assert.That(list.OrEmpty(), Is.Empty);
        }

        [Test]
        public void OrEmptyReturnsSelfWhenItsNotNull() {
            IEnumerable<string> list = new[] { "a", "b", "c" };
            Assert.That(list.OrEmpty(), Is.EqualTo(list));
        }

        [Test]
        public void OrEmptyReturnsEmptyStringWhenSelfIsNull() {
            string text = null;
            Assert.That(text.OrEmpty(), Is.EqualTo(string.Empty));
        }

        [Test]
        public void OrEmptyReturnsSelfWhenItsNotNullString() {
            string text = "a text";
            Assert.That(text.OrEmpty(), Is.EqualTo(text));
        }

        [Test]
        public void ShouldReturnRandomItem() {
            var list = new[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            var count = 5000;
            var counters = list
                .Select(x => new KeyValuePair<int, int>(x, 0))
                .ToDictionary(x => x.Key, x => x.Value);
            for (int i = 0; i < count; i++) {
                var extracted = list.Random();
                counters[extracted] += 1;
            }
            Assert.That(counters.Values.Min(), Is.GreaterThan(0.80 * count / list.Length));
            Assert.That(counters.Values.Max(), Is.LessThan(1.20 * count / list.Length));
        }
        
        [Test]
        public void RandomShouldReturnDefaultvalueWhenListIsNull() {
            IList<int> list = null;
            Assert.That(list.Random(), Is.EqualTo(default(int)));
        }
        
        [Test]
        public void RandomShouldReturnDefaultvalueWhenListIsEmpty() {
            IList<int> list = Enumerable.Empty<int>().ToList();
            Assert.That(list.Random(), Is.EqualTo(default(int)));
        }
    }
}
