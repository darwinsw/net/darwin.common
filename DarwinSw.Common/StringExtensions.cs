namespace Darwin.Common {
    public static class ObjectExtensions {
        public static T Or<T>(this T self, T defaultValue) {
            return self ?? defaultValue;
        }
    }
}
