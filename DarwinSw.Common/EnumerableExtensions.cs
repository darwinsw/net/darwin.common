﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Darwin.Common {
    public static class EnumerableExtensions {
        public static IEnumerable<TValue> OrEmpty<TValue>(this IEnumerable<TValue> self) {
            return self ?? Enumerable.Empty<TValue>();
        }
        
        private static readonly Random Rnd = new Random();

        public static TValue Random<TValue>(this IList<TValue> self) {
            if (self == null || !self.Any()) {
                return default(TValue);
            }
            return self[Rnd.Next(0, self.Count)];
        }
    }
    
    
    
    
}
