# Darwin.Common
A collection of very basic, common utility methods you can resuse everywhere in your .Net code.

| Branch | Pipeline Status                                                                                                                                                | Code Coverage                                                                                                                                                           |
|--------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| main   | [![pipeline status](https://gitlab.com/darwinsw/net/darwin.common/badges/main/pipeline.svg)](https://gitlab.com/darwinsw/net/darwin.common/commits/main) | [![coverage](https://gitlab.com/darwinsw/net/darwin.common/badges/main/coverage.svg)](https://darwinsw.gitlab.io/net/darwin.common/coverage/main/html-report/index.htm) |
