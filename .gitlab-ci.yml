stages:
  - test
  - reporting
  - pack
  - deploy

variables:
  TOOLS_DIR: "tools"
  COVERAGE_DIR: "coverage"
  SOLUTION_NAME: "DarwinSw.Common"
  PACKAGES_DIR: "target"
  PUBLIC_SITE: "https://darwinsw.gitlab.io/net/darwin.common"
  DEPENDENCIES_DIR: "dependencies"
  CONFIGURATION: "Release"

run_tests:
  stage: test
  image: mcr.microsoft.com/dotnet/sdk:5.0-buster-slim
  script:
    - dotnet restore --packages $DEPENDENCIES_DIR $SOLUTION_NAME.sln
    - dotnet build --configuration $CONFIGURATION $SOLUTION_NAME.sln
    - cd ${SOLUTION_NAME}.Tests
    - dotnet test --configuration $CONFIGURATION $SOLUTION_NAME.Tests.csproj /p:CollectCoverage=true /p:CoverletOutputFormat=opencover /p:CoverletOutput=$COVERAGE_DIR/coverage /p:Include="[$SOLUTION_NAME*]*" /p:Exclude="[nunit.*]*"
  coverage: '/Total\s*\|\s*(\d+(?:\.\d+)?)/'
  artifacts:
    paths:
      - "**/bin/${CONFIGURATION}/**"
      - $DEPENDENCIES_DIR
      - "**/$COVERAGE_DIR"

build_coverage_report:
  stage: reporting
  image: mcr.microsoft.com/dotnet/sdk:5.0-buster-slim
  script:
    - dotnet restore --packages $DEPENDENCIES_DIR $SOLUTION_NAME.sln
    - cd $SOLUTION_NAME.Tests
    - dotnet reportgenerator -reports:$COVERAGE_DIR/*.xml -targetdir:$COVERAGE_DIR/html-report '-assemblyfilters:+DarwinSw.Common;+DarwinSw.Common.*'
  artifacts:
    paths:
      - "**/$COVERAGE_DIR/html-report"
  rules:
    - if: '$CI_COMMIT_TAG'
    - if: '$CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH'

pack_assemblies:
  stage: pack
  image: mcr.microsoft.com/dotnet/sdk:5.0-buster-slim
  artifacts:
    paths:
      - "**/bin/${CONFIGURATION}/*.nupkg"
  script:
    - |
      for prj in $(find . -name $SOLUTION_NAME*.csproj) ; do
        module=$(basename $prj | sed 's/.csproj//')
        echo "Processing ${module}"
        cd $module
        sed -i "s/<VersionPrefix>.*<\/VersionPrefix>/<VersionPrefix>$CI_COMMIT_REF_NAME<\/VersionPrefix>/" ${module}.csproj
        dotnet pack --configuration $CONFIGURATION
      cd ..
      done
  rules:
    - if: '$CI_COMMIT_TAG'

publish_assemblies:
  stage: deploy
  image: mcr.microsoft.com/dotnet/sdk:5.0-buster-slim
  script:
    - for package in $(find . -name ${SOLUTION_NAME}*.nupkg) ; do dotnet nuget push -s https://api.nuget.org/v3/index.json -k ${NUGET_PUSH_KEY} ${package} ; done
  rules:
    - if: '$CI_COMMIT_TAG'

pages:
  stage: deploy
  image: pietrom/my-shell:1.0.0
  dependencies:
    - build_coverage_report
  script:
    - |
      mkdir -p $COVERAGE_DIR/html-report
      touch $COVERAGE_DIR/html-report/pippo.htm
      curl -f -s $PUBLIC_SITE/coverage/coverage.tgz > old-coverage.tgz
      if [ $? -eq 0 ]
      then
        echo "Previous coverage reports acquired"
        ls -al
        if [ -f old-coverage.tgz ] ; then
          echo "Debug 0"
          file old-coverage.tgz | grep gzip || exit_code=$?
          if [ $exit_code -eq 0 ]
          then
            echo "Debug 1"
            echo "Deflating gzip file"
            tar xzf old-coverage.tgz
          else
            echo "Debug 2"
            echo "No gzip file - discarding it"
          fi
          echo "Debug 3"
        fi        
      else
        echo "Debug 4"
        echo "No previous coverage reports found"
      fi
      echo "Debug 5"
      rm -rf public/coverage/$CI_COMMIT_REF_NAME
      mkdir -p public/coverage/$CI_COMMIT_REF_NAME
      mv $SOLUTION_NAME.Tests/$COVERAGE_DIR/html-report public/coverage/$CI_COMMIT_REF_NAME
      tar czf coverage.tgz public
      mv coverage.tgz public/coverage
  artifacts:
    paths:
      - public
  rules:
    - if: '$CI_COMMIT_TAG'
    - if: '$CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH'
